# KediHealth

 ##  ABOUT KediHealth
KEDI Healthcare has established itself as a world-class, and leading global provider of healthcare products and equipment. It is a conglomerate with business interests in pharmaceutical sectors in China that promotes Traditional Chinese Medical Culture worldwide.
see [kedihealth.com](https://www.kedihealth.com/)

## Preview
[Preview this project on](https://sandykedistore.com/)

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
